<?php
namespace Drupal\commerce_cnp\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Yaml;
use \SoapClient;
use SimpleXMLElement;
use Drupal\Core\Ajax\AjaxResponse;
use DOMDocument;
use Exception;
/**
 * Provides the Test payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "cnpmoney_redirect",
 *   label = "Click&Pledge",
 *   display_label = "Click&Pledge",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_cnp\PluginForm\CnPCommerceRedirect\PaymentCnPMoneyForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   js_library = "commerce_cnp/js",
 * )
 */
 

class CnPCommerceRedirect extends OnsitePaymentGatewayBase
{
	 /**
   * Convenientstore test API URL for Initial access.
   */
  const TEST_URL = 'https://api-test.com';
  const LIVE_URL = 'https://api.live.com';
  
  protected $order;
  protected $orderTransation;
  protected $amount;
  protected $refund_amount;
  protected $payment;
    /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,$plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $current_path = \Drupal::service('path.current')->getPath();

    $args = explode("/", $current_path);
    foreach ($args as $key => $value) {
      if (is_numeric($value)) {
        $orderId = $value;
        $this->order = \Drupal\commerce_order\Entity\Order::load($orderId);
       // $this->orderTransation = "check" . $this->order->id() . "-" . time();
      }
    }
  }
  /**
   * {@inheritdoc}
   */
  public function getSecurity_key() {
    //return $this->configuration['security_key'];
  }


  /**
   * {@inheritdoc}
   */
  public function getMerchant_id() {
    //return $this->configuration['merchant_id'];
  }


  /**
   * {@inheritdoc}
   */
  /*public function defaultConfiguration() {
    return [
		'cnpkey' => '',
		'cnpsalt' => '',
    ] + parent::defaultConfiguration();
  }*/
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
	$form = parent::buildConfigurationForm($form, $form_state);
	
    return $this->crateCnPConfigurationForm($form,$form_state); 
  }
   public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    
   }
   /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    /*if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['cnpkey'] = $values['cnpkey'];
      $this->configuration['cnpsalt'] = $values['cnpsalt'];
    }*/
  }
   /**
   * {@inheritdoc}
   */
   public function onReturn(OrderInterface $order, Request $request) {
	//exit("On Return");
    $additionalCharges = $request->get('additionalCharges');
    $status = $request->get('status');
    $firstname = $request->get('firstname');
    $txnid = $request->get('txnid');
    $amount = $request->get('amount');
    $posted_hash = $request->get('hash');
    $key = $request->get('key');
    $productinfo = $request->get('productinfo');
    $email = $request->get('email');
    $salt = $this->configuration['cnpsalt'];

    if (isset($additionalCharges)) {
      $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
    }
    else {
      $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
    }

    $hash = hash("sha512", $retHashSeq);

    if ($hash != $posted_hash) {
      drupal_set_message($this->t('Invalid Transaction. Please try again'), 'error');
    }
    else {
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => $status,
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->entityId,
        'order_id' => $order->id(),
        'test' => $this->getMode() == 'test',
        'remote_id' => $txnid,
        'remote_state' => $request->get('payment_status'),
        'authorized' => REQUEST_TIME,
      ]);
      $payment->save();
      drupal_set_message($this->t('Your payment was successful with Order id : @orderid and Transaction id : @transaction_id', ['@orderid' => $order->id(), '@transaction_id' => $txnid]));
    }
  }
   protected function getShipment() {
    /** @var ShipmentItem $shipments */
	//exit(\Drupal::moduleHandler()->moduleExists('commerce_shipping'));
	if (\Drupal::moduleHandler()->moduleExists('commerce_shipping') && $this->order->hasField('shipments') && !($this->order->get('shipments')->isEmpty())) {
    
	$shipments = $this->order->get('shipments');
    if ($shipments->isEmpty()) {
      // Temporary code to create a new shipment
      $shipment = $this->createShipment();

     $shipments->entity = $shipment;

      $this->order->save();
    } else {
      $shipment = $shipments->entity;
    }
   }
   else
   {
	   $shipment="";
   }

    return $shipment;
}
  //CREATE PAYMENT
   public function createPayment(PaymentInterface $payment, $capture = TRUE) {
	
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
	$this->assertPaymentMethod($payment_method);
	//exit("create Payment");
	
	if($this->order->getTotalPrice()->isZero())
	{
		$pM='';
	}
	else
	{
		$pM=$payment_method->getRemoteId();
	}
	
    $amount = $payment->getAmount();
    $transaction_data = [
      'currency' => $amount->getCurrencyCode(),
      'amount' => $this->toMinorUnits($amount),
      'source' => $pM,
      'capture' => $capture,
    ];

	if(!$this->order->getTotalPrice()->isZero())
	{
		//echo "Here";
		$owner = $payment_method->getOwner();
		
		if ($owner && $owner->isAuthenticated()) {
		  $transaction_data['customer'] = $this->getRemoteCustomerId($owner);
		}
	}
	
    $mode = $payment_method->getPaymentGatewayMode();
	//var_dump($mode);
	//exit();
    $payment_gateway_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $payment_gateway = $payment_gateway_storage->load('cnp');
   
    $payment_details = $_SESSION['payment_details'];
	//print_r($payment_details);
	
    $amount =  $payment->getAmount()->getNumber();
    $month = (string)$payment_details['expiration']['month'];
    $year = (string)$payment_details['expiration']['year'];
    $year = substr($year, -2);
    $exp_date = $month . $year;
    //$security_key = $this->configuration['security_key'];
    //$merchant_id = $this->configuration['merchant_id'];
  
    //$string = $security_key . ":";
    //$base = base64_encode($string);
    $name = $this->order->getStore()->getName();
    
    /*$reqArray = [
      "merchant_id" => "",
      "card_number" => $payment_details['number'],
      "exp_date" => $exp_date,
      "amt_tran" => $amount,
     // "purchase_id" => $this->order->id(),
      "cardholder_name" => $payment_details['owner'],
      "developer_id" => 'addwebdrupalver#',
      "moto_ecomm_ind" => 7 
    ];*/

    //$data_string = json_encode($reqArray);
    /*if($mode == "test") {
      $URL = self::TEST_URL . '/pg/auth';
    } 
    else {
      $URL = self::LIVE_URL . '/pg/auth';
    }*/
	//echo "<pre>";
	
	/*foreach($this->order->getItems() as $it)
	{
		print_r($it->getPurchasedEntity()->sku->value);
	}*/
	//foreach()
	
	
	$connection= \Drupal::database();
	$config = \Drupal::config('cnp.mainsettings');
	$order_id = \Drupal::routeMatch()->getParameter('commerce_order')->id();
    //$order = Order::load($order_id);
	
	
	if($config->get('cnp.cnp_pre_auth')==1 && $payment_details['fef_payment_options']=="Recurring")
	{
		//$this->order->getTotalPrice()->isZero()
		if($this->order->getTotalPrice()->getNumber()==0)
		{
			try{
					if($this->order->getTotalPrice()->getNumber()==0)
					{
						throw new Exception('Zero Payment not allowed with Recurring.Please select One Time Only');      
					}
				}
				catch(Exception $e) {
					 //to delete payment method on failure transactions
					/*$this->order->get("payment_gateway")->setValue(NULL); 
				   $this->order->get("payment_method")->setValue(NULL); 
				   if(!$payment_method->isReusable() || $payment_method->getOwner()->isAnonymous())
				   {
					   $payment_method->delete();
				   }
					*/
					drupal_set_message($e->getMessage(), 'error');
					$this->redirectToPreviousStep();
				}
		}
	}
	
	if($config->get('cnp.cnp_pre_auth')==0)
	{
		if($this->order->getTotalPrice()->getNumber()==0)
		{
			try{
					if($this->order->getTotalPrice()->getNumber()==0)
					{
						throw new Exception('Zero Payment not allowed');      
					}
				}
				catch(Exception $e) {
					 //to delete payment method on failure transactions
					/*$this->order->get("payment_gateway")->setValue(NULL); 
				   $this->order->get("payment_method")->setValue(NULL); 
				   if(!$payment_method->isReusable() || $payment_method->getOwner()->isAnonymous())
				   {
					   $payment_method->delete();
				   }
					*/
					drupal_set_message($e->getMessage(), 'error');
					$this->redirectToPreviousStep();
				}
		}
	}
	
	
	
	//$shipment = $this->getShipment();
	//print_r($shipment->getTitle());
	//print_r($shipment->getOrder()->getTotalPrice()->getNumber());
	
	//$shipping_profile = $shipment->getShippingProfile();
	//print_r($shipping_profile->get('address')->first()->getAddressLine1());
	//print_r($shipping_profile->get('address')->first()->getAddressLine2());
	//print_r($shipping_profile->get('address')->first()->getPostalCode());
	//print_r($shipping_profile->get('address')->first()->getConstraints());
	//print_r($this->order->getBillingProfile()->get('address'));
    //$this->order->get
    //$address=$this->order->getBillingProfile()->address->first();
    $address=$this->order->getBillingProfile()->address->first();
	
	
    $dom = new DOMDocument('1.0', 'UTF-8');
    $root = $dom->createElement('CnPAPI', '');
    $root->setAttribute("xmlns", "urn:APISchema.xsd");
    $root = $dom->appendChild($root);
	
    $version = $dom->createElement("Version", "1.0");
    $version = $root->appendChild($version);
  
    $engine = $dom->createElement('Engine', '');
    $engine = $root->appendChild($engine);
	
    $application = $dom->createElement('Application', '');
    $application = $engine->appendChild($application);

    $applicationid = $dom->createElement('ID', 'CnP_Drupal_Commerce');
    $applicationid = $application->appendChild($applicationid);
	
    $applicationname = $dom->createElement('Name', 'CnP_Drupal_Commerce');
    $applicationid = $application->appendChild($applicationname);

	//Getting drupal Version
	$drupalVersion=\Drupal::VERSION;
	
	
	//Getting Commerce Version
	$path = drupal_get_path('module', "commerce") . '/commerce.info.yml';
	if(file_exists($path))
	{
		$file_path = DRUPAL_ROOT."/";
		$fullpath=$file_path.$path;
		$file_contents = file_get_contents($fullpath);
		//echo "<pre>";
		$ymldata = Yaml::decode($file_contents);
		//print_r();
		$commerceVersion=$ymldata['version'];
	}
	else
	{
		$commerceVersion="";
	}
    $versionString="2.000.000/Drupal:v".$drupalVersion."/DrupalCommerce:v".$commerceVersion;
    $applicationversion = $dom->createElement('Version', $versionString);  // 2.000.000.000.20130103 Version-Minor change-Bug Fix-Internal Release Number -Release Date
    $applicationversion = $application->appendChild($applicationversion);
	
    $request = $dom->createElement('Request', '');
    $request = $engine->appendChild($request);
   
    $operation = $dom->createElement('Operation', '');
    $operation = $request->appendChild( $operation );
	
    $operationtype = $dom->createElement('OperationType', 'Transaction');
    $operationtype = $operation->appendChild($operationtype);

    $ipaddress = $dom->createElement('IPAddress', $this->order->getIpAddress());
    $ipaddress = $operation->appendChild($ipaddress);
	
    $httpreferrer=$dom->createElement('UrlReferrer',$_SERVER['HTTP_REFERER']);
    $httpreferrer=$operation->appendChild($httpreferrer);
	
    $authentication = $dom->createElement('Authentication', '');
    $authentication = $request->appendChild($authentication);
	
	//getting account guid form database table
	$prefix=$connection->tablePrefix();
	$table_name = $prefix.'dp_cnp_dp_jbcnpaccountsinfo';
	$accid=$config->get('cnp.cnp_accid');
	$sql = "SELECT * FROM " .$table_name." where cnpaccountsinfo_orgid='$accid'";
	$query = $connection->query($sql);
	$row=$query->fetchAll();
	//print_r($row);
	//exit();
    $accounttype = $dom->createElement('AccountGuid', $row[0]->cnpaccountsinfo_accountguid );
    $accounttype = $authentication->appendChild($accounttype);

    $accountid = $dom->createElement('AccountID', $config->get('cnp.cnp_accid'));
    $accountid = $authentication->appendChild($accountid);
    
    $cporder = $dom->createElement('Order', '');
    $cporder = $request->appendChild($cporder);
	
	if($config->get('cnp.cnp_mode')=="No")
	{
		$orderMode="Production";
	}
	else
	{
		$orderMode="Test";
	}
	
    $ordermode = $dom->createElement('OrderMode', $orderMode);
    $ordermode = $cporder->appendChild($ordermode);
    if($config->get('cnp.cnp_camp_urls'))
	{
		$camp_url=$config->get('cnp.cnp_camp_urls');
	}
	else
	{
		$camp_url="";
	}
    //$connectcampalias = $dom->createElement('ConnectCampaignAlias', "Feed The poor");
    $connectcampalias = $dom->createElement('ConnectCampaignAlias', $this->safeString($camp_url,50));
    $connectcampalias = $cporder->appendChild($connectcampalias);
	
    $cardholder = $dom->createElement('CardHolder', '');
    $cardholder = $cporder->appendChild($cardholder);
    
    $billinginfo = $dom->createElement('BillingInformation', '');
    $billinginfo = $cardholder->appendChild($billinginfo);
	
	$billfirst_name = $dom->createElement('BillingFirstName', $this->safeString($address->getGivenName(),50));
    $billfirst_name = $billinginfo->appendChild($billfirst_name);

    $billlast_name = $dom->createElement('BillingLastName', $this->safeString($address->getFamilyName(),50));
    $billlast_name = $billinginfo->appendChild($billlast_name);
   
    $bill_email = $dom->createElement('BillingEmail', $this->order->getEmail());
    $bill_email = $billinginfo->appendChild($bill_email);
	
	//$bill_phone=$dom->createElement('BillingPhone',"");
	//$bill_phone=$billinginfo->appendChild($bill_phone);
	
	$billingaddress=$dom->createElement('BillingAddress','');
	$billingaddress=$cardholder->appendChild($billingaddress);
	
	$billingaddress1=$dom->createElement('BillingAddress1',$this->safeString($address->getAddressLine1(),100));
	$billingaddress1=$billingaddress->appendChild($billingaddress1);
	
	if(!empty($address->getAddressLine2())) {
		$billingaddress2=$dom->createElement('BillingAddress2',$this->safeString($address->getAddressLine2(),100));
		$billingaddress2=$billingaddress->appendChild($billingaddress2);
	}
		
	if(!empty($address->getLocality())) {
	$billing_city=$dom->createElement('BillingCity',$this->safeString($address->getLocality(),50));
	$billing_city=$billingaddress->appendChild($billing_city);
	}

	if(!empty($address->getAdministrativeArea())) {
	$billing_state=$dom->createElement('BillingStateProvince',$this->safeString($address->getAdministrativeArea(),50));
	$billing_state=$billingaddress->appendChild($billing_state);
	}
			
	if(!empty($address->getPostalCode())) {
	$billing_zip=$dom->createElement('BillingPostalCode',$this->safeString( $address->getPostalCode(),20 ));
	$billing_zip=$billingaddress->appendChild($billing_zip);
	}
	
	
	$ModulePath = \Drupal::moduleHandler()->getModule('commerce_cnp')->getPath();
	$host = \Drupal::request()->getHost();
	//$host= \Drupal::request()->getSchemeAndHttpHost();
	$countries = simplexml_load_file("http://".$host."/".base_path().$ModulePath."/countries/Countries.xml");
	
	//$countries = simplexml_load_file("http://dev.clickandpledge.biz/web/PHP/ram/drupal-8.5.4/modules/commerce_cnp/countries/Countries.xml");
	//var_dump($countries);
	
	$billing_country_id = '';
	foreach( $countries as $country ){
		if( $country->attributes()->Abbrev == $address->getCountryCode() ){
			$billing_country_id = $country->attributes()->Code;
		} 
	}
	
	if(!empty($address->getCountryCode())) {
	$billing_country=$dom->createElement('BillingCountryCode',str_pad($billing_country_id, 3, "0", STR_PAD_LEFT));
	$billing_country=$billingaddress->appendChild($billing_country);
	} 
	
	//if shipping address found add to XML
	
	//print_r($shipping_profile->get('address')->first()->getPostalCode());
	//$shipadd = $this->order->get('shipments')->referencedEntities();
	
	if (\Drupal::moduleHandler()->moduleExists('commerce_shipping') && $this->order->hasField('shipments') && !($this->order->get('shipments')->isEmpty())) {
		
		$shipment = $this->getShipment();
		
		if($shipment != "")
		{
			$shipping_profile = $shipment->getShippingProfile();
			
			$givenName=$shipping_profile->get('address')->first()->getGivenName();
			$getFamilyName=$shipping_profile->get('address')->first()->getFamilyName();
			$getAddressLine1=$shipping_profile->get('address')->first()->getAddressLine1();
			$getAddressLine2=$shipping_profile->get('address')->first()->getAddressLine2();
			$getCity=$shipping_profile->get('address')->first()->getLocality();
			
			$shippinginfo = $dom->createElement('ShippingInformation', '');
			$shippinginfo = $cardholder->appendChild($shippinginfo);
			
			//Newly Added
			$ShippingContactInformation=$dom->createElement('ShippingContactInformation','');
			$ShippingContactInformation=$shippinginfo->appendChild($ShippingContactInformation);
			
			if($givenName != "")
			{
				$shipping_first_name=$dom->createElement('ShippingFirstName',$this->safeString($givenName,50));
				$shipping_first_name=$ShippingContactInformation->appendChild($shipping_first_name);
			}
			if($getFamilyName != "")
			{
				$shipping_last_name=$dom->createElement('ShippingLastName',$this->safeString($getFamilyName,50));
				$shipping_last_name=$ShippingContactInformation->appendChild($shipping_last_name);
			}
			
			$shippingaddress=$dom->createElement('ShippingAddress','');
			$shippingaddress=$shippinginfo->appendChild($shippingaddress);
			if($getAddressLine1 != "")
			{
				$ship_address1=$dom->createElement('ShippingAddress1',$this->safeString($getAddressLine1,100));
				$ship_address1=$shippingaddress->appendChild($ship_address1);
			}
			if($getAddressLine2 != "")
			{
				$ship_address2=$dom->createElement('ShippingAddress2',$this->safeString($getAddressLine2,100));
				$ship_address2=$shippingaddress->appendChild($ship_address2);
			}
			if($getCity != "")
			{
				$ship_city=$dom->createElement('ShippingCity',$this->safeString($getCity, 50));
				$ship_city=$shippingaddress->appendChild($ship_city);
			}
			
			$getAdministrativeArea=$shipping_profile->get('address')->first()->getAdministrativeArea();
			if($getAdministrativeArea != "")
			{
				$ship_state=$dom->createElement('ShippingStateProvince',$this->safeString($getAdministrativeArea, 50));
				$ship_state=$shippingaddress->appendChild($ship_state);
			}
			
			$getPostalCode=$shipping_profile->get('address')->first()->getPostalCode();
			if($getPostalCode != "")
			{
				$ship_zip=$dom->createElement('ShippingPostalCode',$this->safeString($getPostalCode, 20));
				$ship_zip=$shippingaddress->appendChild($ship_zip);
			}
			
			$getCountryCode=$shipping_profile->get('address')->first()->getCountryCode();
			
			$shipping_country_id = '';
			foreach( $countries as $country ){
				if( $country->attributes()->Abbrev == $address->getCountryCode() ){
					$shipping_country_id = $country->attributes()->Code;
				} 
			}
			
			if($getCountryCode != "")
			{
				$ship_country=$dom->createElement('ShippingCountryCode',str_pad($shipping_country_id, 3, "0", STR_PAD_LEFT));
				$ship_country=$shippingaddress->appendChild($ship_country);
			}
		}
		
	}
	//billing company name
	if($address->getOrganization()!="")
	{
		$custom_fields['Billing Company Name'] = $address->getOrganization();
	}
	//shipping company name
	if (\Drupal::moduleHandler()->moduleExists('commerce_shipping') && $this->order->hasField('shipments') && !($this->order->get('shipments')->isEmpty())) {
		$shipment = $this->getShipment();
		$shipping_profile = $shipment->getShippingProfile();
		if($shipping_profile->get('address')->first()->getOrganization()!="")
		{
			$custom_fields['Shipping Company Name']=$shipping_profile->get('address')->first()->getOrganization();
		}
	}
	if(count($custom_fields) > 0) {
		
		$customfieldlist = $dom->createElement('CustomFieldList','');
		$customfieldlist = $cardholder->appendChild($customfieldlist);
		foreach($custom_fields as $cfk=>$cfv)
		{
			$customfield = $dom->createElement('CustomField','');
			$customfield = $customfieldlist->appendChild($customfield);
			
			$fieldname   = $dom->createElement('FieldName',$this->safeString($cfk, 200));
			$fieldname   = $customfield->appendChild($fieldname);
			$fieldvalue  = $dom->createElement('FieldValue',$this->safeString($cfv, 500));
			$fieldvalue  = $customfield->appendChild($fieldvalue);
		}
	}
    $paymentmethod = $dom->createElement('PaymentMethod', '');
    $paymentmethod = $cardholder->appendChild($paymentmethod);
	
    $PaymentType=$payment_details['fef_payment_methods'];
    if($PaymentType=="Credit Card")
    {
		
        $payment_type = $dom->createElement('PaymentType', 'CreditCard');
        $payment_type = $paymentmethod->appendChild($payment_type);
		
        $creditcard = $dom->createElement('CreditCard', '');
        $creditcard = $paymentmethod->appendChild($creditcard);
		
		if($payment_details['owner']!=""){
			$credit_name = $dom->createElement('NameOnCard',$this->safeString($payment_details['owner'],50));
			$credit_name = $creditcard->appendChild($credit_name);
		}
		else
		{
			$fname=$address->getGivenName();
			$lname=$address->getFamilyName();
			$owner=$fname." ".$lname;
			$credit_name = $dom->createElement('NameOnCard',$this->safeString($owner,50));
			$credit_name = $creditcard->appendChild($credit_name);
		}
		//print_r($payment_details);
		$month = (string)$payment_details['expiration']['month'];
		$year = (string)$payment_details['expiration']['year'];
		$year = substr($year, -2);
		$exp_date = $month ."/". $year;
		
        $credit_number = $dom->createElement('CardNumber', $this->safeString( str_replace(' ', '', $payment_details['cardnumber']), 19));
        //$credit_number = $dom->createElement('CardNumber', $this->safeString( str_replace(' ', '', '4111111111111111'), 17));
        $credit_number = $creditcard->appendChild($credit_number);
		
        $credit_cvv = $dom->createElement('Cvv2', $payment_details['security_code']);
        $credit_cvv = $creditcard->appendChild($credit_cvv);

        $credit_expdate = $dom->createElement('ExpirationDate', $exp_date);
        $credit_expdate = $creditcard->appendChild($credit_expdate);
    }
    else if($PaymentType=="eCheck")
    {
		//echo "<pre>";
		//print_r($values);
        $payment_type = $dom->createElement('PaymentType', 'Check');
        $payment_type = $paymentmethod->appendChild($payment_type);
        
        $echeck=$dom->createElement('Check','');
        $echeck=$paymentmethod->appendChild($echeck);
        
        $ecAccount=$dom->createElement('AccountNumber',$this->safeString( $payment_details['cnp_fef_account_number'], 17));
        $ecAccount=$echeck->appendChild($ecAccount);
        
        $ecAccount_type=$dom->createElement('AccountType',$values['cnp_fef_account_type']);
        $ecAccount_type=$echeck->appendChild($ecAccount_type);
        
        $ecRouting=$dom->createElement('RoutingNumber',$this->safeString( $payment_details['cnp_fef_routing_number'], 9));
        $ecRouting=$echeck->appendChild($ecRouting);
        
        $ecCheck=$dom->createElement('CheckNumber',$this->safeString( $payment_details['cnp_fef_check_number'], 10));
        $ecCheck=$echeck->appendChild($ecCheck);
        
        $ecChecktype=$dom->createElement('CheckType',$payment_details['cnp_fef_check_type']);
        $ecChecktype=$echeck->appendChild($ecChecktype);
        
        $ecName=$dom->createElement('NameOnAccount',$this->safeString( $payment_details['cnp_fef_name_on_account'], 100));
        $ecName=$echeck->appendChild($ecName);
        
        $ecIdtype=$dom->createElement('IdType',$values['cnp_fef_type_of_id']);
        $ecIdtype=$echeck->appendChild($ecIdtype);
        
    }
    else{
        $payment_type=$dom->createElement('PaymentType','CustomPaymentType');
        $payment_type=$paymentmethod->appendChild($payment_type);
        
        $CustomPayment=$dom->createElement('CustomPaymentType','');
        $CustomPayment=$paymentmethod->appendChild($CustomPayment);
        
        $CustomPaymentNum=$dom->createElement('CustomPaymentNumber',$this->safeString($payment_details['cnp_fef_reference_code'],50));
        $CustomPaymentNum=$CustomPayment->appendChild($CustomPaymentNum);
                        
    }
	
    $orderitemlist = $dom->createElement('OrderItemList', '');
    $orderitemlist = $cporder->appendChild($orderitemlist);
	
	 //iterate Order Items
    $UnitPriceCalculate = $UnitTaxCalculate = $ShippingValueCalculate = $ShippingTaxCalculate = $TotalDiscountCalculate = 0;	
    $pi=103000;
    $grandTotalPrice="";
    //echo $coupon = $form_state->getValue(['coupon', 'code']);
    foreach($this->order->getItems() as $it)
    {
		++$pi;
        $pv=$it->getPurchasedEntity();
        $adjustments = $it->getAdjustments();
        
        $negative_tax_adjustments = array_filter($adjustments, function ($adjustment) {
          /** @var \Drupal\commerce_order\Adjustment $adjustment */
          return $adjustment->getType() == 'tax' && $adjustment->isNegative();
        });
        $adjustments = array_diff_key($adjustments, $negative_tax_adjustments);
        
        $orderitem = $dom->createElement('OrderItem', '');
        $orderitem = $orderitemlist->appendChild($orderitem);

        $itemid = $dom->createElement('ItemID', $pi);
        $itemid = $orderitem->appendChild($itemid);

        $itemname = $dom->createElement('ItemName', $it->get('title')->value);
        $itemname = $orderitem->appendChild($itemname);

        $quntity = $dom->createElement('Quantity', abs($it->get('quantity')->value));
        $quntity = $orderitem->appendChild($quntity);
		
        //recurring calculation for unit price
        //echo $pv->getPrice()->getNumber();
		
		if($payment_details['fef_payment_options']=="Recurring")
		{
			if($payment_details['fef_recuring_type_option']=="Installment")
			{
				//installment
				if($payment_details['fef_cnp_noof_payments']=="Indefinite Recurring Only")
				{
					$no_of_payments=$payment_details['fef_cnp_noof_payments'];
					$Unit_Price = ($this->number_formatprc(($pv->getPrice()->getNumber()/999),2,'.','')*100);
					$UnitPriceCalculate += ($this->number_formatprc(($pv->getPrice()->getNumber()/999),2,'.','')*$it->get('quantity')->value);
				}
				else
				{
					//for selected no.of payments
					$no_of_payments=$payment_details['fef_cnp_noof_payments'];
					$Unit_Price=($this->number_formatprc(($pv->getPrice()->getNumber()/$no_of_payments),2,'.','')*100);
					$UnitPriceCalculate += ($this->number_formatprc(($pv->getPrice()->getNumber()/$no_of_payments),2,'.','')*$it->get('quantity')->value);
				}
				$unitprice = $dom->createElement('UnitPrice', $Unit_Price);
				$unitprice = $orderitem->appendChild($unitprice);
				
			}
			else
			{
				//subscription
				$unitprice = $dom->createElement('UnitPrice', $this->number_formatprc($pv->getPrice()->getNumber(),2,'.','')*100);
				$unitprice = $orderitem->appendChild($unitprice);
				$UnitPriceCalculate += ($pv->getPrice()->getNumber()*$it->get('quantity')->value);
			}
		}
		else
		{
			//One Time Only
			$unitprice = $dom->createElement('UnitPrice', $this->number_formatprc($pv->getPrice()->getNumber(),2,'.','')*100);
			$unitprice = $orderitem->appendChild($unitprice);
			$UnitPriceCalculate += ($pv->getPrice()->getNumber()*$it->get('quantity')->value);
		}
        
		//print_r($this->order->collectAdjustments());
		
        //recurring calculation for Tax and Discounts
        if(count($adjustments)>0)
        {
            $totalTax=0;
            $totalDiscount=0;
            $unitD=0;
            foreach($adjustments as $adjustment)
            {
				//print_r($adjustment->getType()."<br>");
                if($adjustment->getType()=="tax")
                {
					/*$sourceString = $adjustment->getSourceId();
					list($tax_type_id, $zoneId, $rateId) = explode('|', $sourceString);
				   $tax_type = $this->entityTypeManager->getStorage('commerce_tax_type')->load($tax_type_id);
				   */
                    $indTax=$adjustment->getAmount()->getNumber();
                    $percentage=$adjustment->getPercentage()*100;
                    //echo "Tax:".$percentage."<br>";
					if($payment_details['fef_payment_options']=="Recurring")
					{
						if($payment_details['fef_recuring_type_option']=="Installment")
						{
							//installment
							if($payment_details['fef_cnp_noof_payments']=="Indefinite Recurring Only")
							{
								$no_of_payments=$payment_details['fef_cnp_noof_payments'];
								$Unit_Tax = $this->number_formatprc(($indTax/999),2,'.','')*100;
								$UnitTaxCalculate += ($this->number_formatprc(($indTax/999),2,'.','')*$it->get('quantity')->value);
							}
							else
							{
								//for selected no.of payments
								$no_of_payments=$payment_details['fef_cnp_noof_payments'];
								$Unit_Tax = $this->number_formatprc(($indTax/$no_of_payments),2,'.','')*100;
								$UnitTaxCalculate += ($this->number_formatprc(($indTax/$no_of_payments),2,'.','')*$it->get('quantity')->value);
								
							}
							$itemtax = $dom->createElement('UnitTax',$Unit_Tax);
							$itemtax = $orderitem->appendChild($itemtax);
							
						}
						else
						{
							//if subscription
							$itemtax = $dom->createElement('UnitTax',$this->number_formatprc($indTax,2,'.','')*100);
							$itemtax = $orderitem->appendChild($itemtax);
							$UnitTaxCalculate += ($this->number_formatprc($indTax,2,'.','')*$it->get('quantity')->value);
						}
					}
					else
					{
						//echo "if one time only:".$indTax."<br>";
						
						$itemtax = $dom->createElement('UnitTax',$this->number_formatprc($indTax,2,'.','')*100);
						$itemtax = $orderitem->appendChild($itemtax);
						$UnitTaxCalculate += ($this->number_formatprc($indTax,2,'.','')*$it->get('quantity')->value);
					}
					
					
					
					
					
                }
				//discount calculation $TotalDiscountCalculate=0
                
				
				
            }
            //$adjusted_unit_price=$it->getAdjustedUnitPrice(['promotion', 'fee']);
            
            //$adjusted_total_price=$it->getAdjustedTotalPrice()->getNumber();
            //$grandTotalPrice += $adjusted_total_price;
			
			foreach($adjustments as $adjustment)
            {
			if($adjustment->getType()=="promotion")
                {   
                   
					$IndItemDiscount=abs($adjustment->getAmount()->getNumber());
					
					if($payment_details['fef_payment_options']=="Recurring")
					{
						if($payment_details['fef_recuring_type_option']=="Installment")
						{
							//echo "Here1";
							//installment
							if($payment_details['fef_cnp_noof_payments']=="Indefinite Recurring Only")
							{
								$no_of_payments=$payment_details['fef_cnp_noof_payments'];
								$Unit_Discount = $this->number_formatprc(($IndItemDiscount/999),2,'.','')*100;
								$TotalDiscountCalculate += ($this->number_formatprc(($IndItemDiscount/999),2,'.','')*$it->get('quantity')->value);
							}
							else
							{
								//for selected no.of payments
								$no_of_payments=$payment_details['fef_cnp_noof_payments'];
								$Unit_Discount = $this->number_formatprc(($IndItemDiscount/$no_of_payments),2,'.','')*100;
								$TotalDiscountCalculate += ($this->number_formatprc(($IndItemDiscount/$no_of_payments),2,'.','')*$it->get('quantity')->value);
								
							}
							$unititemdiscount = $dom->createElement('UnitDiscount',$Unit_Discount);
							$unititemdiscount = $orderitem->appendChild($unititemdiscount);
							
						}
						else
						{
							//if subscription
							//echo "Here2";
							$unititemdiscount = $dom->createElement('UnitDiscount',$this->number_formatprc($IndItemDiscount,2,'.','')*100);
							$unititemdiscount = $orderitem->appendChild($unititemdiscount);
							$TotalDiscountCalculate += ($this->number_formatprc($IndItemDiscount,2,'.','')*$it->get('quantity')->value);
						}
					}
					else
					{
						//if one time only
						//exit("Here3");
						
							if($this->order->getTotalPrice()->isZero())
							{
								
								
								if($unitD === 1)
								{
									//echo "Here";
									$unititemdiscount = $dom->createElement('UnitDiscount',$this->number_formatprc($IndItemDiscount,2,'.','')*100);
									$unititemdiscount = $orderitem->appendChild($unititemdiscount);
									$TotalDiscountCalculate += ($this->number_formatprc($IndItemDiscount,2,'.','')*$it->get('quantity')->value);
								}
							}
							else
							{
								$unititemdiscount = $dom->createElement('UnitDiscount',$this->number_formatprc($IndItemDiscount,2,'.','')*100);
								$unititemdiscount = $orderitem->appendChild($unititemdiscount);
								$TotalDiscountCalculate += ($this->number_formatprc($IndItemDiscount,2,'.','')*$it->get('quantity')->value);
							}
					}
					
					
					
                }
				
				$unitD++;
			}
			
        }
        else
        {
            //if no adjustments
            $totalTax=0;
            $totalDiscount=0;
			$no_of_ietms=count($this->order->getItems());
            $grandTotalPrice += $this->number_formatprc($this->order->getTotalPrice()->getNumber(),2,'.','');
        }
        
        //$pi++;
		$sku_code=$dom->createElement('SKU',$this->safeString($it->getPurchasedEntity()->sku->value, 100));
		$sku_code=$orderitem->appendChild($sku_code);
		
    }//products foreach loop end
	
	//print_r($dom->saveXML());
	//echo "<pre>";
	//var_dump($shipping_profile->get('address')->first());
	//exit("getting extra fields");
	
	
	//add sku field;$it->getPurchasedEntity()->sku->value;
	
	//calculate discounts at subtotal level or Discount at cart level
	//print_r($this->order->collectAdjustments());
    $totalItems=count($this->order->getItems());
	//getAdjustments()
	
    if(count($this->order->getAdjustments())>0)
    {
        foreach($this->order->getAdjustments() as $adjustment)
        {
			if($adjustment->getType()=="tax")
			{
				
				//$UnitTaxCalculate += $this->number_formatprc($adjustment->getAmount()->getNumber(),2,'.','')*$totalItems;
				if($payment_details['fef_payment_options']=="Recurring")
				{
					if($payment_details['fef_recuring_type_option']=="Installment")
					{
						//installment
						if($payment_details['fef_cnp_noof_payments']=="Indefinite Recurring Only")
						{
							$no_of_payments=$payment_details['fef_cnp_noof_payments'];
							$UnitTaxCalculate += ($this->number_formatprc(($adjustment->getAmount()->getNumber()/999),2,'.','')*$totalItems);
						}
						else
						{
							//for selected no.of payments
							$no_of_payments=$payment_details['fef_cnp_noof_payments'];
							$UnitTaxCalculate += ($this->number_formatprc(($adjustment->getAmount()->getNumber()/$no_of_payments),2,'.','')*$totalItems);
						}
					}
					else
					{
						//if subscription
						$UnitTaxCalculate += ($this->number_formatprc($adjustment->getAmount()->getNumber(),2,'.','')*$totalItems);
					}
				}
				else
				{
					//if one time only
					$UnitTaxCalculate += ($this->number_formatprc($adjustment->getAmount()->getNumber(),2,'.','')*$totalItems);
				}
			}
			else if($adjustment->getType()=="promotion")
			{
				//$TotalDiscountCalculate += $this->number_formatprc($adjustment->getAmount()->getNumber(),2,'.','')*$totalItems;
				if($payment_details['fef_payment_options']=="Recurring")
				{
					if($payment_details['fef_recuring_type_option']=="Installment")
					{
						//installment
						if($payment_details['fef_cnp_noof_payments']=="Indefinite Recurring Only")
						{
							$no_of_payments=$payment_details['fef_cnp_noof_payments'];
							$TotalDiscountCalculate += ($this->number_formatprc(($adjustment->getAmount()->getNumber()/999),2,'.','')*$totalItems);
						}
						else
						{
							//for selected no.of payments
							$no_of_payments=$payment_details['fef_cnp_noof_payments'];
							$TotalDiscountCalculate += ($this->number_formatprc(($adjustment->getAmount()->getNumber()/$no_of_payments),2,'.','')*$totalItems);
						}
					}
					else
					{
						//if subscription
						$TotalDiscountCalculate += ($this->number_formatprc($adjustment->getAmount()->getNumber(),2,'.','')*$totalItems);
					}
				}
				else
				{
					//if one time only
					$TotalDiscountCalculate += ($this->number_formatprc($adjustment->getAmount()->getNumber(),2,'.','')*$totalItems);
				}
			}
			else if($adjustment->getType()=="shipping")
			{
				//$ShippingValueCalculate += $this->number_formatprc($adjustment->getAmount()->getNumber(),2,'.','')*$totalItems;
				if($payment_details['fef_payment_options']=="Recurring")
				{
					if($payment_details['fef_recuring_type_option']=="Installment")
					{
						//echo "Ins";
						//installment
						if($payment_details['fef_cnp_noof_payments']=="Indefinite Recurring Only")
						{
							$no_of_payments=$payment_details['fef_cnp_noof_payments'];
							$ShippingValueCalculate += ($this->number_formatprc(($adjustment->getAmount()->getNumber()/999),2,'.','')*$totalItems);
						}
						else
						{
							//for selected no.of payments
							$no_of_payments=$payment_details['fef_cnp_noof_payments'];
							$ShippingValueCalculate += ($this->number_formatprc(($adjustment->getAmount()->getNumber()/$no_of_payments),2,'.','')*$totalItems);
						}
					}
					else
					{
						//if subscription
						//echo "Sub";
						$ShippingValueCalculate += ($this->number_formatprc($adjustment->getAmount()->getNumber(),2,'.','')*$totalItems);
					}
				}
				else
				{
					//if one time only
					$ShippingValueCalculate += ($this->number_formatprc($adjustment->getAmount()->getNumber(),2,'.','')*$totalItems);
				}
			}
			
        }
		
        //$grandTotalPrice=$grandTotalPrice-$totalDiscount;
    }
    else
    {
        //$order->getTotalPrice()->getNumber();
    }
	//print_r($payment_details);
	//print_r($ShippingValueCalculate);
	//exit();
	 //$ShippingValueCalculate
	//if($ShippingValueCalculate!="")

		

    if (\Drupal::moduleHandler()->moduleExists('commerce_shipping') && $this->order->hasField('shipments') && !($this->order->get('shipments')->isEmpty())) {
        $shipment = $this->getShipment();
        $shipping=$dom->createElement('Shipping','');
        $shipping=$cporder->appendChild($shipping);

        $shipping_method=$dom->createElement('ShippingMethod',$this->safeString($shipment->getTitle(),50));
        $shipping_method=$shipping->appendChild($shipping_method);

        $shipping_value = $dom->createElement('ShippingValue', $ShippingValueCalculate*100);
        $shipping_value=$shipping->appendChild($shipping_value);
    }
	
    $receipt = $dom->createElement('Receipt', '');
    $receipt = $cporder->appendChild($receipt);
	
    if($config->get('cnp.cnp_receipt_patron')[1]==1)
	{
		$SendReceipt="true";
	}
	else
	{
		$SendReceipt="false";
	}
    $sendreceipt=$dom->createElement("SendReceipt",$SendReceipt);
    $sendreceipt=$receipt->appendChild($sendreceipt);
    
    $language=$dom->createElement("Language","ENG");
    $language=$receipt->appendChild($language);
    
	//collect receipt t&c and header information
	
	if($config->get('cnp.cnp_receipt_header')!="")
	{
		$receiptHeader=$config->get('cnp.cnp_receipt_header');
	}
	else
	{
		$receiptHeader="";
	}
	
    /*$orginfo=$dom->createElement("OrganizationInformation",$receiptHeader);
    $orginfo=$receipt->appendChild($orginfo);*/
    
	$orginfo=$dom->createElement("OrganizationInformation",'');
        $orginfo=$receipt->appendChild($orginfo);
	$orginfo->appendChild($dom->createCDATASection($receiptHeader));
	
	if($config->get('cnp.cnp_terms_con')!="")
	{
		$receipttnc=$config->get('cnp.cnp_terms_con');
	}
	else
	{
		$receipttnc="";
	}
	
    /*$termscon=$dom->createElement("TermsCondition", $receipttnc);
    $termscon=$receipt->appendChild($termscon);
    */
	
	$termscon=$dom->createElement("TermsCondition",'');
        $termscon=$receipt->appendChild($termscon);
	$termscon->appendChild($dom->createCDATASection($receipttnc));
	
    $emailnotilist=$dom->createElement("EmailNotificationList","");
    $emailnotilist=$receipt->appendChild($emailnotilist);
   
    $notificationemail=$dom->createElement("NotificationEmail","");
    $notificationemail=$emailnotilist->appendChild($notificationemail);
    
    $transaction = $dom->createElement('Transaction', '');
    $transaction = $cporder->appendChild($transaction);
	
	if($payment_details['fef_payment_methods'] == 'CreditCard' ) {			
            if($this->order->getTotalPrice()->getNumber()==0) {
                $trans_type=$dom->createElement('TransactionType','PreAuthorization');
                $trans_type=$transaction->appendChild($trans_type);
            } else {
                $trans_type=$dom->createElement('TransactionType','Payment');
                $trans_type=$transaction->appendChild($trans_type);
            }
	} else {
		if($this->order->getTotalPrice()->getNumber()==0)
		{
			$trans_type=$dom->createElement('TransactionType','PreAuthorization');
			$trans_type=$transaction->appendChild($trans_type);
		}
		else
		{
			$trans_type=$dom->createElement('TransactionType','Payment');
			$trans_type=$transaction->appendChild($trans_type);
		}
	}
    
    $trans_desc = $dom->createElement('DynamicDescriptor', 'DynamicDescriptor');
    $trans_desc = $transaction->appendChild($trans_desc);
    
	if($payment_details['fef_payment_options']=="Recurring")
	{
		$trans_recurr=$dom->createElement('Recurring','');
		$trans_recurr=$transaction->appendChild($trans_recurr);
		
		if($payment_details['fef_recuring_type_option']=="Installment")
		{
			//installment
			if($payment_details['fef_cnp_noof_payments']=="Indefinite Recurring Only")
			{
				$no_of_payments=$payment_details['fef_cnp_noof_payments'];
				$total_installment=$dom->createElement('Installment',998);
				$total_installment=$trans_recurr->appendChild($total_installment);
			}
			else
			{
				//for selected no.of payments
				$no_of_payments=$payment_details['fef_cnp_noof_payments'];
				if($no_of_payments!="")
				{
					$total_installment=$dom->createElement('Installment',$no_of_payments);
					$total_installment=$trans_recurr->appendChild($total_installment);
				}
				else
				{
					$total_installment=$dom->createElement('Installment',1);
					$total_installment=$trans_recurr->appendChild($total_installment);
				}
			}
			$total_periodicity=$dom->createElement('Periodicity',$payment_details['fef_cnp_periodcity']);
			$total_periodicity=$trans_recurr->appendChild($total_periodicity);
			
			$RecurringMethod=$dom->createElement('RecurringMethod',$payment_details['fef_recuring_type_option']);
			$RecurringMethod=$trans_recurr->appendChild($RecurringMethod);
		}
		else
		{
			if($payment_details['fef_cnp_noof_payments']=="Indefinite Recurring Only")
			{
				$no_of_payments=$payment_details['fef_cnp_noof_payments'];
				$total_installment=$dom->createElement('Installment',999);
				$total_installment=$trans_recurr->appendChild($total_installment);
			}
			else
			{
				//for selected no.of payments
				$no_of_payments=$payment_details['fef_cnp_noof_payments'];
				if($no_of_payments!="")
				{
					$total_installment=$dom->createElement('Installment',$no_of_payments);
					$total_installment=$trans_recurr->appendChild($total_installment);
				}
				else
				{
					$total_installment=$dom->createElement('Installment',1);
					$total_installment=$trans_recurr->appendChild($total_installment);
				}
			}
			$total_periodicity=$dom->createElement('Periodicity',$payment_details['fef_cnp_periodcity']);
			$total_periodicity=$trans_recurr->appendChild($total_periodicity);
			
			$RecurringMethod=$dom->createElement('RecurringMethod',"Subscription");
			$RecurringMethod=$trans_recurr->appendChild($RecurringMethod);
		}
	
	}
	
    $trans_totals = $dom->createElement('CurrentTotals', '');
    $trans_totals = $transaction->appendChild($trans_totals);
    
    if($TotalDiscountCalculate)
    {
        $total_discount = $dom->createElement( 'TotalDiscount',  $TotalDiscountCalculate*100);
        $total_discount = $trans_totals->appendChild($total_discount);
    }
    if($UnitTaxCalculate)
    {
        $total_tax = $dom->createElement( 'TotalTax',  $UnitTaxCalculate*100);
        $total_tax = $trans_totals->appendChild($total_tax);
    }
	if($ShippingValueCalculate!="")
	{
		$TotalShipping=$dom->createElement('TotalShipping',$ShippingValueCalculate*100);
		$TotalShipping=$trans_totals->appendChild($TotalShipping);
	}
	
    
	//echo $gtAmount=$this->order->getTotalPrice()->getNumber();
	$gtAmount = ( $this->number_formatprc($UnitPriceCalculate, 2, '.', '')+  $this->number_formatprc($UnitTaxCalculate, 2, '.', '') + $this->number_formatprc($ShippingValueCalculate, 2, '.', ''))  - ($TotalDiscountCalculate );
	
	
	if($payment_details['fef_payment_options']=="Recurring")
	{
		if($payment_details['fef_recuring_type_option']=="Installment")
		{
			//installment
			if($payment_details['fef_cnp_noof_payments']=="Indefinite Recurring Only")
			{
				$no_of_payments=$payment_details['fef_cnp_noof_payments'];
				$total_amount = $dom->createElement( 'Total', ($this->number_formatprc(($gtAmount),2,'.','')*100)  );
				$total_amount = $trans_totals->appendChild($total_amount);
			}
			else
			{
				//for selected no.of payments
				$no_of_payments=$payment_details['fef_cnp_noof_payments'];
				$total_amount = $dom->createElement( 'Total',  ($this->number_formatprc(($gtAmount),2,'.','')*100) );
				$total_amount = $trans_totals->appendChild($total_amount);
			}
		}
		else
		{
			//if subscription
			$total_amount = $dom->createElement( 'Total',  ($this->number_formatprc($gtAmount,2,'.','')*100) );
			$total_amount = $trans_totals->appendChild($total_amount);
		}
	}
	else
	{
		//if one time only
		$total_amount = $dom->createElement( 'Total',  ($this->number_formatprc($gtAmount,2,'.','')*100) );
		$total_amount = $trans_totals->appendChild($total_amount);
	}
	if(Order::load($this->order->id())->get('coupons')[0])
	{
	/*if($TotalDiscountCalculate!="")
	{*/
		
		$couponCode=Order::load($this->order->id())->get('coupons')[0]->entity->getCode();
		$trans_coupon = $dom->createElement('CouponCode', $couponCode);
		$trans_coupon = $transaction->appendChild($trans_coupon);
	}
	//print_r($payment_details);
	
		$strParam=$dom->saveXML();
		
		//print_r("<br>".$this->order->getTotalPrice()->getNumber()."<br>");
		
		//print_r($strParam);
		//exit();
		$payment_method_token = $payment_method->getRemoteId();
		$next_state = $capture ? 'completed' : 'authorization';
		//print_r($next_state);
		
		
		
		
		
			$connect    = array('soap_version' => SOAP_1_1, 'trace' => 1, 'exceptions' => 0);
			$client     = new SoapClient('https://paas.cloud.clickandpledge.com/paymentservice.svc?wsdl', $connect);
			$soapParams = array('instruction'=>$strParam);
			
			$response = $client->Operation($soapParams);
			$pg_id = $payment->getRemoteId();
			//echo "<pre>";
			//print_r($response->OperationResult);
			//print_r($response->OperationResult->ResultCode);
			//exit();
			if($response->OperationResult->ResultCode==0)
			{
				$payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
				$payment = $payment_storage->create([
				  'state' => 'authorization',
				  'amount' => $this->order->getTotalPrice(),
				  'payment_gateway' => $this->entityId,
				  'order_id' => $this->order->id(),
				  'test' => $this->getMode() == 'test',
				  'remote_id' => $this->order->id(),
				  'remote_state' => 'Success',
				  'authorized' => $this->time->getRequestTime(),
				]);
				$payment->save();
				$_SESSION['payment_details']="";
				$successMsg=$response->OperationResult->ResultData;
				$txID=$response->OperationResult->TransactionNumber;
				drupal_set_message($this->t('Your payment was successful with Order id : @orderid and Transaction id : @transaction_id', ['@orderid' => $this->order->id(), '@transaction_id' => $txID]));
			}
			else
			{
				 try{
					if($response->OperationResult->ResultCode!=0)
					{
						$errorMsg=$response->OperationResult->ResultData;
						//$_SESSION['payment_details'] = $payment_details;
						//throw new Exception('We encountered an error processing your payment. Please verify your details and try again.');      
						throw new Exception($errorMsg.'. Please verify your details and try again.');      
					}
				}
				catch(Exception $e) {
					 //to delete payment method on failure transactions
					$this->order->get("payment_gateway")->setValue(NULL); 
				   $this->order->get("payment_method")->setValue(NULL); 
				   if(!$payment_method->isReusable() || $payment_method->getOwner()->isAnonymous())
				   {
					   //$payment_method->delete();
				   }
					
					drupal_set_message($e->getMessage(), 'error');
					$this->redirectToPreviousStep();
				}
				
			}
		
  }
  
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
	//exit("Capture Payment");
    // If not specified, capture the entire amount.
    $pg_id = $payment->getRemoteId();
    $mode = $payment->getPaymentGatewayMode();
    $amt_tran = $payment->getAmount()->getNumber();
    $security_key = $this->configuration['security_key'];
    $merchant_id = $this->configuration['merchant_id'];
    $string = $security_key . ":";
    $base = base64_encode($string);

    $reqArray = [
      "merchant_id" => $merchant_id,
      "amt_tran" => $amt_tran
    ];
    
    $data_string = json_encode($reqArray); 
    if($mode == "test") {
      $URL = self::TEST_URL . '/pg/capture/'. $pg_id;      
    }
    else {
      $URL = self::LIVE_URL . '/pg/capture/'. $pg_id;      
    }

    $ch = curl_init($URL);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json',
        'Authorization:Basic ' . $base
      ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    $result_Array = json_decode($result);

    if($result_Array->rcode != "000") {
      $this->capturePayment($payment, $amount);
    }
    else {
      $payment->setState('completed');
      $payment->setAmount($amount);
      $payment->save();
      \Drupal::logger('commerce_cnp')->notice('captchured: ' . $result);
    }
  }

  
  
/**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $_SESSION['payment_details'] = $payment_details;
    
  
	
		$expires = CreditCard::calculateExpirationTimestamp(
		  $payment_details['expiration']['month'],
		  $payment_details['expiration']['year']
		);
		
		$payment_method->setExpiresTime($expires);
		// Set the payment method as not reusable.
		// @todo Allow configuring whether the payment methods should be reusable.
		$payment_method->setReusable(FALSE);
		$payment_method->save();
	
  }
/**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
		$payment_method->delete();
  }
  /**
   * {@inheritdoc}
   */
	public function onCancel(OrderInterface $order, Request $request) {
		$status = $request->get('status');
		drupal_set_message($this->t('Payment @status on @gateway but may resume the checkout process here when you are ready.', [
			  '@status' => $status,
			  '@gateway' => $this->getDisplayLabel(),
			]), 'error');
	}
	public function crateCnPConfigurationForm($form,$form_state)
	{
		/*$form['some_text1'] = array(
            '#markup' => '<H3><u>CnP Settings</u></H3>'
        );*/
		
		$form['base_url_cnp'] = [
			'#type' => 'hidden',
			'#default_value' => base_path(),
			'#attributes' => array("id"=>"base_url_cnp"),
		];
		
		/*$form['cnpkey'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Security Key'),
			'#default_value' => $this->configuration['cnpkey'],
		];
		$form['cnpsalt'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Security Salt'),
			'#default_value' => $this->configuration['cnpsalt'],
		];*/
		
		
		
		
		
		//$my_config = \Drupal::config('cnp.mainsettings')->get('cnp_accid');
		//print_r("Data:".$my_config);
		return $form;
	}
	public function safeString( $str,  $length=1, $start=0 )
	{
		$str = preg_replace('/\x03/', '', $str); //Remove new line characters
		return substr( htmlspecialchars( ( $str ) ), $start, $length );
	}
	public function number_formatprc($number, $decimals = 2,$decsep = '', $ths_sep = '') {
		$parts = explode('.', $number);
		if(count($parts) > 1) {	return $parts[0].'.'.substr($parts[1],0,$decimals);	} else {return $number;	}
	}
	protected function redirectToPreviousStep() {
        //$payment_info_pane = $this->checkoutFlow->getPane('payment_information');
        //$previous_step_id = $payment_info_pane->getStepId();
        //$this->checkoutFlow->redirectToStep($previous_step_id);
        throw new NeedsRedirectException($_SERVER['HTTP_REFERER']);
        
	}

}