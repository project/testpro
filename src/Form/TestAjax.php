<?php
//https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21InvokeCommand.php/class/InvokeCommand/8.2.x
namespace Drupal\commerce_cnp\Form;  
use Drupal\Core\Form\ConfigFormBase;  
use Drupal\Core\Form\FormStateInterface;  
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;

class TestAjax extends ConfigFormBase
{
    /**  
    * {@inheritdoc}  
    */ 
   protected function getEditableConfigNames() {
       return [  
          'cnp.examples' 
        ]; 
   }
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "cnp_testajax";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
    
    $form['test'] = [
    '#type' => 'button',
    '#value' => 'test Now',
    '#ajax' => [
      'callback' => [$this,'ajaxTesting'],
      'event' => 'click',
        'progress' => [
           'type' => 'throbber',
           'message' => 'Getting some thing...',
          ],
        ],
      ];

    return parent::buildForm($form, $form_state);  
   }
  
    /**
   * {@inheritdoc}
   */
    public function validateForm(array &$form, FormStateInterface $form_state) {
     
    }
	
   
   public function submitForm(array &$form, FormStateInterface $form_state) {
        parent::submitForm($form, $form_state);
        
       
   }
   public function ajaxTesting(){
        $ajax_response = new AjaxResponse();
        $ajax_response->addCommand(new InvokeCommand(NULL, 'myTest', ['some Var']));
        return $ajax_response;
   }
  
    
    
    
}