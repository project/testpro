<?php

/*namespace Drupal\commerce_payumoney\PluginForm\PayUMoneyRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\Order;
*/
namespace Drupal\commerce_cnp\PluginForm\CnPCommerceRedirect;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use DOMDocument;

class PaymentCnPMoneyForm extends BasePaymentMethodAddForm
{
    //const QUALPAY_TEST_URL = 'https://api-test.qualpay.com';
    
    /**
   * {@inheritdoc}
   */
 /**
   * {@inheritdoc}
   */
  public function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    
    // Set our key to settings array.
    /** @var \Drupal\commerce_qualpay\Plugin\Commerce\PaymentGateway\QualpayInterface $plugin */
    $plugin = $this->plugin; 
    // Build a month select list that shows months with a leading zero.
	$textmonths = array(
    1=>'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July ',
    'August',
    'September',
    'October',
    'November',
    'December',
	);
    $months = [];
    for ($i = 1; $i < 13; $i++) {
      $month = str_pad($i, 2, '0', STR_PAD_LEFT);
      $months[$month] = $textmonths[$i]." (".$month.")";
    }
    // Build a year select list that uses a 4 digit key with a 2 digit value.
    $current_year_4 = date('Y');
    $current_year_2 = date('Y');
    $years = [];
    for ($i = 0; $i < 20; $i++) {
      $years[$current_year_4 + $i] = $current_year_2 + $i;
    }

	$config = \Drupal::config('cnp.mainsettings');
        // Will print 'Hello'.
	//print $config->get('cnp.cnp_vemail');
	$element['#attributes']['class'][] = 'credit-card-form';
	//print_r($config->get('cnp.cnp_status_enable')['yes']);
	
	if($config->get('cnp.cnp_status_enable')['yes']==="yes")
	{
	
	$element['cnp_fef_front_end_form_start'] = array(
		'#prefix' => '<div class="cnp_fef_front_end">',
	);
		
	
	
	$element['fef_some_text_start'] = array(
		'#prefix' => '<div class="cnp_payment_heading"><p>Secured credit card payment with Click and Pledge API.</p>',
		'#suffix' => '</div>',
	);
	
	$cnplogo="<img alt='Click&Pledge Secured' src='".base_path().drupal_get_path('module', 'commerce_cnp')."/images/logo.jpg'>";
	
	$element['cnp_logo'] = array(
		'#prefix' => '<div class="cnp_logo">'.$cnplogo,
		'#suffix' => '</div>',
	);
	$PayOption="";
	if(isset($_POST['payment_information']['add_payment_method']['payment_details']['fef_payment_options']))
	{
		$PayOption=$_POST['payment_information']['add_payment_method']['payment_details']['fef_payment_options'];
		if($PayOption=="Recurring")
		{
			$element['cnppayoption'] = [
				'#type' => 'hidden',
				'#value' => $PayOption,
				"#attributes"=>array("id"=>"cnppayoption"),
			];
		}
		else
		{
			$element['cnppayoption'] = [
				'#type' => 'hidden',
				'#value' => $PayOption,
				"#attributes"=>array("id"=>"cnppayoption"),
			];
		}
	}
	else
	{
		$element['cnppayoption'] = [
				'#type' => 'hidden',
				'#value' => $PayOption,
				"#attributes"=>array("id"=>"cnppayoption"),
			];
	}
	
	//echo $config->get('cnp.cnp_recurr_oto')['oto'];
	if($config->get('cnp.cnp_recurr_oto')['oto']==="oto" && $config->get('cnp.cnp_recurr_recur')[1]==1)
	{
		$element['fef_recurring_payment_lbl'] = array(
		//'#prefix' => '<div class=""><p><b>Set this as a recurring payment</b></p>',
		'#prefix' => '<div class="cnp_form_recurring_label"><p><b>'.$config->get('cnp.cnp_recurr_label').'</b></p>',
		'#suffix' => '</div>',
		);
		$element['fef_payment_options_lbl'] = array(
			//'#prefix' => '<div class=""><p><b>Payment Options *</b></p>',
			'#prefix' => '<div class="cnp_form_recurr_settings"><p><b>'.$config->get('cnp.cnp_recurr_settings').' <span class="cnpstar_color">*</span></b></p>',
			'#suffix' => '</div>',
		);
		$element['cnp_form_payment_options_start'] = array(
			'#prefix' => '<div class="cnp_form_payment_options">',
		);
			 $element['fef_payment_options'] = [
				'#type' => 'radios',
				'#attributes' => array("data-payment-options"=>"fef_payment_options"),
				//'#required' => TRUE,
				"#options"=>array("One Time Only"=>"One Time Only","Recurring"=>"Recurring"),
				"#prefix"=>'<div class="container-inline">',
				"#suffix"=>'</div>',
				"#default_value"=>$config->get('cnp.cnp_default_payment_options'),
				'#ajax' => [
					'callback' => [$this, 'displayRecurringForm'],
				],
			];
		$element['cnp_form_payment_options_end'] = array(
			'#suffix' => '</div>',
		);
		//toggle to display recurring options form
		if($config->get('cnp.cnp_default_payment_options')==="Recurring")
		{
			$cssClass="recurring_div_show";
		}
		else
		{
			$cssClass="recurring_div_hide";
		}
		//echo $hideOpt;
		
		/*if($config->get('cnp.cnp_default_payment_options')!=="One Time Only")
		{*/
		
		$element['fef_recurr_options_div_starts'] = array(
		'#prefix' => '<div class="'.$cssClass.'" id="fef_recurr_options_division">'
		);
			
			//check recurring type checkbox values. if both are checked,display dropdown with both
			//options or else display individually
			
			$rto=$config->get('cnp.cnp_recurr_type_option');
			
			if($rto['Installment']==="Installment" && $rto['Subscription']==="Subscription")
			{
				$element['cnp_form_fef_recuring_type_option_start'] = array(
					'#prefix' => '<div class="cnp_form_recuring_type_option">',
				);
				
					$element['fef_recuring_type_option'] = [
						'#type' => 'select',
						'#title' => t($config->get('cnp.cnp_recurring_types').'<span style="color:red"> *</span>'),
						'#options' =>$rto,
						"#default_value"=>$config->get('cnp.cnp_default_recurring_type'),
					];
				
				$element['cnp_form_fef_recuring_type_option_end'] = array(
					'#suffix' => '</div>',
				);
			}
			else
			{
				/*$element['fef_recurr_options_label_display'] = array(
				'#prefix' => '<div class="cnp_form_recurr_options_label"><p>'.$config->get('cnp.cnp_recurring_types').': '.$config->get('cnp.cnp_default_recurring_type').'</p>',
				'#suffix' => '</div>',
				);
				$element['fef_recuring_type_option'] = [
				  '#type' => 'hidden',
				  '#value' => $config->get('cnp.cnp_default_recurring_type'),
				];*/
				$othopt=$config->get('cnp.cnp_recurr_type_option');
				
				if($othopt['Installment']==="Installment" && $othopt['Subscription']===0)
				{
					$element['fef_recurr_options_label_display'] = array(
					'#prefix' => '<div class="cnp_form_recurr_options_label"><p>'.$config->get('cnp.cnp_recurring_types').': '.$othopt['Installment'].'</p>',
					'#suffix' => '</div>',
					);
					$element['fef_recuring_type_option'] = [
					  '#type' => 'hidden',
					  '#value' => $othopt['Installment'],
					];
				}
				else if($othopt['Installment']===0 && $othopt['Subscription']==="Subscription")
				{
					$element['fef_recurr_options_label_display'] = array(
					'#prefix' => '<div class="cnp_form_recurr_options_label"><p>'.$config->get('cnp.cnp_recurring_types').': '.$othopt['Subscription'].'</p>',
					'#suffix' => '</div>',
					);
					$element['fef_recuring_type_option'] = [
					  '#type' => 'hidden',
					  '#value' => $othopt['Subscription'],
					];
				}
			}
			//DISPLAY PERIODCITY: IF IT IS ONE OPTION DISPLAY DIRECTLY OR MULTIPLE OPTIONS
			//DISPLAY ALL OPTIONS AS DROPDOWN
			$periodcityOpt=$config->get('cnp.cnp_recurring_periodicity_options');
			$periodOpt=array();
			foreach($periodcityOpt as $popts)
			{
				if($popts !== 0)
				{
					$periodOpt[$popts]=$popts;
				}
			}
			if(count($periodOpt)==1)
			{
				
				$element['fef_cnp_periodcity_label_display'] = array(
				'#prefix' => '<div class="cnp_form_periodcity_label"><p>'.$config->get('cnp.cnp_recurring_periodicity').': '.array_values($periodOpt)[0].'</p>',
				'#suffix' => '</div>',
				);
				$element['fef_cnp_periodcity'] = [
				  '#type' => 'hidden',
				  '#value' => array_values($periodOpt)[0],
				];
				
			}
			else
			{
				$element['cnp_form_periodcity_start'] = array(
					'#prefix' => '<div class="cnp_form_periodcity">',
				);


					$element['fef_cnp_periodcity'] = [
						'#type' => 'select',
						'#title' => t($config->get('cnp.cnp_recurring_periodicity').'<span style="color:red"> *</span>'),
						'#options' =>$periodOpt,
					];
				
				$element['cnp_form_periodcity_end'] = array(
					'#suffix' => '</div>',
				);
				
			}
		//display default no.of payments based on no.of payments radio buttons
		//echo $config->get('cnp.cnp_recurring_default_no_payments_open_filed');
		
		$nop=$config->get('cnp.cnp_recurring_no_of_payments_options');
		if($nop==="fixednumber")
		{
			//echo "fixednumber";
			$config->get('cnp.cnp_recurring_default_no_payment_fncc_lbl');
			$element['fef_cnp_noof_payments_label_display'] = array(
				'#prefix' => '<div class="cnp_form_noof_payments_label"><p>'.$config->get('cnp.cnp_recurring_no_of_payments').': '.$config->get('cnp.cnp_recurring_default_no_payments_fnnc').'</p>',
				'#suffix' => '</div>',
			);
			$element['fef_cnp_noof_payments'] = [
				  '#type' => 'hidden',
				  '#default_value' => $config->get('cnp.cnp_recurring_default_no_payments_fnnc'),
				];
		}
		elseif($nop==="indefinite_openfield")
		{
			//echo $config->get('cnp.cnp_recurring_default_no_payments_open_filed');
			//echo $config->get('cnp.cnp_recurring_max_no_payment_open_filed');
			
			$element['cnp_form_noof_payments_start'] = array(
				'#prefix' => '<div class="cnp_form_noof_payments">',
			);

			$element['fef_cnp_noof_payments'] = [
				  '#type' => 'number',
				  //"#required"=>true,
				  "#title"=>t($config->get('cnp.cnp_recurring_no_of_payments'))."<span class='redcolor'>*</span>",
				  '#default_value' =>$config->get('cnp.cnp_recurring_default_no_payments_open_filed'),
			];
			
			$element['cnp_form_noof_payments_end'] = array(
				'#suffix' => '</div>',
			);
			
		}
		elseif($nop==="openfield")
		{
			//echo $config->get('cnp.cnp_recurring_default_no_payments');
			//echo $config->get('cnp.cnp_recurring_max_no_payment');
			
			$element['cnp_form_noof_payments_start'] = array(
				'#prefix' => '<div class="cnp_form_noof_payments">',
			);
			
			$element['fef_cnp_noof_payments'] = [
				  '#type' => 'number',
				  //"#required"=>true,
				  "#title"=>t($config->get('cnp.cnp_recurring_no_of_payments'))." <span class='redcolor'>*</span>",
				  '#default_value' =>$config->get('cnp.cnp_recurring_default_no_payments'),
			];	
			
			$element['cnp_form_noof_payments_end'] = array(
				'#suffix' => '</div>',
			);
		}
		else
		{
			//$config->get('cnp.cnp_recurring_default_no_payment_fncc_lbl');
			$element['fef_cnp_noof_payments_label_display'] = array(
				'#prefix' => '<div class="cnp_form_noof_payments_label"><p>'.$config->get('cnp.cnp_recurring_no_of_payments').': <b>Indefinite Recurring Only</b></p>',
				'#suffix' => '</div>',
			);
			$element['fef_cnp_noof_payments'] = [
				  '#type' => 'hidden',
				  '#default_value' =>"Indefinite Recurring Only",
				];
		}
		
		$element['fef_recurr_options_div_end'] = array(
		'#suffix' => '</div>',
		);
		
		//}
	}
	else if($config->get('cnp.cnp_recurr_oto')['oto']===0 && $config->get('cnp.cnp_recurr_recur')[1]==1)
	{
		
		$element['fef_recurring_payment_lbl'] = array(
		//'#prefix' => '<div class=""><p><b>Set this as a recurring payment</b></p>',
		'#prefix' => '<div class="cnp_form_recurring_label"><p><b>'.$config->get('cnp.cnp_recurr_label').'</b></p>',
		'#suffix' => '</div>',
		);
		 $element['fef_payment_options'] = [
			'#type' => 'hidden',
			//'#required' => TRUE,
			"#value"=>$config->get('cnp.cnp_default_payment_options'),
		];
		
		
		/*if($config->get('cnp.cnp_default_payment_options')!=="One Time Only")
		{*/
		
		$element['fef_recurr_options_div_starts'] = array(
		'#prefix' => '<div class="" id="fef_recurr_options_division">'
		);
			
			//check recurring type checkbox values. if both are checked,display dropdown with both
			//options or else display individually
			
			$rto=$config->get('cnp.cnp_recurr_type_option');
			
			if($rto['Installment']==="Installment" && $rto['Subscription']==="Subscription")
			{
				$element['cnp_form_fef_recuring_type_option_start'] = array(
					'#prefix' => '<div class="cnp_form_recuring_type_option">',
				);
					$element['fef_recuring_type_option'] = [
						'#type' => 'select',
						'#title' => t($config->get('cnp.cnp_recurring_types').'<span style="color:red"> *</span>'),
						'#options' =>$rto,
						"#default_value"=>$config->get('cnp.cnp_default_recurring_type'),
					];
				$element['cnp_form_fef_recuring_type_option_end'] = array(
					'#suffix' => '</div>',
				);
			}
			else
			{
				$othopt=$config->get('cnp.cnp_recurr_type_option');
				
				if($othopt['Installment']==="Installment" && $othopt['Subscription']===0)
				{
					$element['fef_recurr_options_label_display'] = array(
					'#prefix' => '<div class="cnp_form_recurr_options_label"><p>'.$config->get('cnp.cnp_recurring_types').': '.$othopt['Installment'].'</p>',
					'#suffix' => '</div>',
					);
					$element['fef_recuring_type_option'] = [
					  '#type' => 'hidden',
					  '#value' => $othopt['Installment'],
					];
				}
				else if($othopt['Installment']===0 && $othopt['Subscription']==="Subscription")
				{
					$element['fef_recurr_options_label_display'] = array(
					'#prefix' => '<div class="cnp_form_recurr_options_label"><p>'.$config->get('cnp.cnp_recurring_types').': '.$othopt['Subscription'].'</p>',
					'#suffix' => '</div>',
					);
					$element['fef_recuring_type_option'] = [
					  '#type' => 'hidden',
					  '#value' => $othopt['Subscription'],
					];
				}
				//if($othopt['Installment'])
				/*$element['fef_recurr_options_label_display'] = array(
				'#prefix' => '<div><p>'.$config->get('cnp.cnp_recurring_types').':'.$config->get('cnp.cnp_default_recurring_type').'</p>',
				'#suffix' => '</div>',
				);
				$element['fef_recuring_type_option'] = [
				  '#type' => 'hidden',
				  '#value' => $config->get('cnp.cnp_default_recurring_type'),
				];*/
			}
			//DISPLAY PERIODCITY: IF IT IS ONE OPTION DISPLAY DIRECTLY OR MULTIPLE OPTIONS
			//DISPLAY ALL OPTIONS AS DROPDOWN
			$periodcityOpt=$config->get('cnp.cnp_recurring_periodicity_options');
			$periodOpt=array();
			foreach($periodcityOpt as $popts)
			{
				if($popts !== 0)
				{
					$periodOpt[$popts]=$popts;
				}
			}
			if(count($periodOpt)==1)
			{
				
				$element['fef_cnp_periodcity_label_display'] = array(
				'#prefix' => '<div class="cnp_form_periodcity_label"><p>'.$config->get('cnp.cnp_recurring_periodicity').': '.array_values($periodOpt)[0].'</p>',
				'#suffix' => '</div>',
				);
				$element['fef_cnp_periodcity'] = [
				  '#type' => 'hidden',
				  '#value' => array_values($periodOpt)[0],
				];
				
			}
			else
			{
				$element['cnp_form_periodcity_start'] = array(
					'#prefix' => '<div class="cnp_form_periodcity">',
				);
				$element['fef_cnp_periodcity'] = [
					'#type' => 'select',
					'#title' => t($config->get('cnp.cnp_recurring_periodicity').'<span style="color:red"> *</span>'),
					'#options' =>$periodOpt,
				];
				$element['cnp_form_periodcity_end'] = array(
					'#prefix' => '</div>',
				);
			}
		//display default no.of payments based on no.of payments radio buttons
		//echo $config->get('cnp.cnp_recurring_default_no_payments_open_filed');
		$nop=$config->get('cnp.cnp_recurring_no_of_payments_options');
		if($nop==="fixednumber")
		{
			//echo "fixednumber";
			$config->get('cnp.cnp_recurring_default_no_payment_fncc_lbl');
			$element['fef_cnp_noof_payments_label_display'] = array(
				'#prefix' => '<div class="cnp_form_noof_payments_label"><p>'.$config->get('cnp.cnp_recurring_no_of_payments').': '.$config->get('cnp.cnp_recurring_default_no_payments_fnnc').'</p>',
				'#suffix' => '</div>',
			);
			$element['fef_cnp_noof_payments'] = [
				  '#type' => 'hidden',
				  '#default_value' => $config->get('cnp.cnp_recurring_default_no_payments_fnnc'),
				];
		}
		elseif($nop==="indefinite_openfield")
		{
			//echo $config->get('cnp.cnp_recurring_default_no_payments_open_filed');
			//echo $config->get('cnp.cnp_recurring_max_no_payment_open_filed');
			
			$element['cnp_form_noof_payments_start'] = array(
				'#prefix' => '<div class="cnp_form_noof_payments">',
			);
			
			$element['fef_cnp_noof_payments'] = [
				  '#type' => 'textfield',
				  "#title"=>t($config->get('cnp.cnp_recurring_no_of_payments')),
				  '#default_value' =>$config->get('cnp.cnp_recurring_default_no_payments_open_filed'),
			];
			
			$element['cnp_form_noof_payments_end'] = array(
				'#suffix' => '</div>',
			);
		}
		elseif($nop==="openfield")
		{
			//echo $config->get('cnp.cnp_recurring_default_no_payments');
			//echo $config->get('cnp.cnp_recurring_max_no_payment');
			$element['cnp_form_noof_payments_start'] = array(
				'#prefix' => '<div class="cnp_form_noof_payments">',
			);
			$element['fef_cnp_noof_payments'] = [
				  '#type' => 'textfield',
				  "#title"=>t($config->get('cnp.cnp_recurring_no_of_payments')),
				  '#default_value' =>$config->get('cnp.cnp_recurring_default_no_payments'),
			];	
			$element['cnp_form_noof_payments_end'] = array(
				'#suffix' => '</div>',
			);
		}
		else
		{
			//$config->get('cnp.cnp_recurring_default_no_payment_fncc_lbl');
			$element['fef_cnp_noof_payments_label_display'] = array(
				'#prefix' => '<div class="cnp_form_noof_payments_label"><p>'.$config->get('cnp.cnp_recurring_no_of_payments').': <b>Indefinite Recurring Only</b></p>',
				'#suffix' => '</div>',
			);
			$element['fef_cnp_noof_payments'] = [
				  '#type' => 'hidden',
				  '#default_value' =>"Indefinite Recurring Only",
				];
		}
		
		$element['fef_recurr_options_div_end'] = array(
		'#suffix' => '</div>',
		);
		//}
	}
	else if($config->get('cnp.cnp_recurr_oto')['oto']==="oto" && $config->get('cnp.cnp_recurr_recur')[1]==0)
	{
		$element['fef_recurring_payment_lbl'] = array(
		//'#prefix' => '<div class=""><p><b>Set this as a recurring payment</b></p>',
		//'#prefix' => '<div class=""><p><b>1234</b></p>',
		//'#suffix' => '</div>',
		);
		 $element['fef_payment_options'] = [
			'#type' => 'hidden',
			"#value"=>"One Time Only",
		];
		
		
	}
	//Wrapper to display recurring and one time form
	
	$element['fef_payment_methods_lbl'] = array(
		'#prefix' => '<div class=""><p></p>',
		'#suffix' => '</div>',
	);
	//get payment methods
	//cnp_payment_echeck_hidden
	$payOpt=array();
	$payOpt["Credit Card"]="Credit Card";
	/*if($config->get('cnp.cnp_payment_echeck_hidden')!="")
	{
		$payOpt['eCheck']=$config->get('cnp.cnp_payment_echeck_hidden');
	}
	//var_dump($config->get('cnp.cnp_payment_methdos1')['CustomPayment']);
	if($config->get('cnp.cnp_payment_methdos1')['CustomPayment']==="CustomPayment")
	{
		
		$others=$config->get('cnp.cnp_customPayment_titles');
		$arrO=explode(";",$others,-1);
		foreach($arrO as $a)
		{
		   $payOpt[$a]=$a;
		}
	}*/
	
	/*$element['fef_payment_methods'] = [
      '#type' => 'radios',
      '#attributes' => array("data-payment-methods"=>"fef_payment_methods"),
      //'#required' => TRUE,
	  "#options"=>$payOpt,
	  "#prefix"=>'<div class="container-inline">',
	  "#suffix"=>'</div>',
	  "#default_value"=>"Credit Card",
	  '#ajax' => [
		  'callback' => [$this, 'displayPaymentForm'],
		  'wrapper' => 'cnp-credit-card-division',
		],
    ];*/
	$element['fef_payment_methods'] = [
      '#type' => 'hidden',
      '#value' => 'Credit Card',
    ];
	 
	
	/* ************************************************************
	************* Credit Card Form starts Here ********************
	***************************************************************/
	
	
	$element['fef_creditcard_div_starts'] = array(
		'#prefix' => '<div class="" id="cnp-credit-card-division">',
	);
	/*$element['type_ele'] = [
      '#type' => 'textfield',
      '#value' => $config->get('cnp.cnp_payment_credit_card_options_hidden'),
    ];*/
	$cimages=array(
		"Amex"=>"amex.jpg",
		"Discover"=>"Discover.jpg",
		"Jcb"=>"JCB.jpg",
		"Master"=>"mastercard.gif",
		"Visa"=>"Visa.jpg",
	);
	
	
	

	$accCards=explode("#",$config->get('cnp.cnp_payment_credit_card_options_hidden'),-1);
	$ipath="";
	foreach($accCards as $cards)
	{
		if(array_key_exists($cards,$cimages)){
			//echo $cimages[$cards];
			$ipath.="<img alt='".$cards."' src='".base_path().drupal_get_path('module', 'commerce_cnp')."/images/$cimages[$cards]'>";
		}
	}
	
	//$ipath=drupal_get_path('module', 'commerce_cnp')."/images/Visa.jpg";
	$element['fef_creditcard_images'] = array(
		'#prefix' => '<div class="cnp_accepted_payment_icons">'.$ipath.'</div>',
	);
	
    $element['type'] = [
      '#type' => 'hidden',
      '#value' => '',
    ];
	
$element['cnp_form_card_owner_start'] = array(
	'#prefix' => '<div class="cnp_form_card_owner">',
);

    $element['owner'] = [
      '#type' => 'textfield',
      '#title' => t('Name on Card'),
      '#attributes' => array('autocomplete' => 'off',"id"=>"dfs"),
      '#required' => TRUE,
      '#maxlength' => 30,
      //'#size' => 30,
    ];
$element['cnp_form_card_owner_end'] = array(
	'#suffix' => '</div>',
);

$element['cnp_form_cardnumber_start'] = array(
	'#prefix' => '<div class="cnp_form_cardnumber">',
);
//
	if($config->get('cnp.cnp_mode')=="Yes")
	{
		$element['cardnumber'] = [
		  '#type' => 'textfield',
		  '#title' => t('Credit Card Number'),
		  '#attributes' => ['autocomplete' => 'off',"disabled"=>"disabled"],
		'#required' => TRUE,
		  '#maxlength' => 16,
		  "#default_value"=>"4111111111111111",
		  //'#size' => 20,
		];
	}
	else
	{
		$element['cardnumber'] = [
		  '#type' => 'textfield',
		  '#title' => t('Credit Card Number'),
		  '#attributes' => ['autocomplete' => 'off'],
			'#required' => TRUE,
		  '#maxlength' => 19,
		  //"#value"=>$config->get('cnp.cnp_mode'),
		  //'#size' => 20,
		];
	}
$element['cnp_form_cardnumber_end'] = array(
	'#suffix' => '</div>',
);
    $element['expiration'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['credit-card-form__expiration'],
      ],
    ];
    $element['expiration']['month'] = [
      '#type' => 'select',
      '#title' => t('Expiration Date'),
      '#options' => $months,
      '#default_value' => date('m'),
      '#required' => TRUE,
	  "#prefix"=>"<div class='only_expiry_date'>",
		"#suffix"=>"</div>",
    ];
    /*$element['expiration']['divider'] = [
      '#type' => 'item',
      '#title' => '',
      '#markup' => '<span class="credit-card-form__divider">&nbsp;</span>',
    ];*/
    $element['expiration']['year'] = [
      '#type' => 'select',
      '#title' => t(''),
      '#options' => $years,
      '#default_value' => $current_year_4,
	 '#required' => TRUE,
	 "#prefix"=>"<div class='only_year'>",
	 "#suffix"=>"</div>",
    ];
	
	$element['cnp_form_security_code_start'] = array(
	'#prefix' => '<div class="cnp_form_security_code">',
);
	
    $element['security_code'] = [
      '#type' => 'textfield',
      '#title' => t('Card Verification (CVV) '),
      '#attributes' => ['autocomplete' => 'off'],
      '#required' => TRUE,
      '#maxlength' => 4,
      //'#size' => 4,
    ];
$element['cnp_form_security_code_end'] = array(
	'#suffix' => '</div>',
);

	$element['is_cc_enabled'] = [
		'#type' => 'hidden',
		'#value' => 'yes',
	];
    // To display validation errors.
    $element['payment_errors'] = [
      '#type' => 'markup',
      '#markup' => '<div id="payment-errors"></div>',
      '#weight' => -200,
    ];

	$element['fef_creditcard_div_ends'] = array(
		'#suffix' => '</div>',
	);
	
	$element['cnp_fef_front_end_form_end'] = array(
		'#suffix' => '</div>',
	);
	
	}
	else
	{
		$element['fef_cnp_creditcard_diabled'] = array(
			'#prefix' => '<div><p><b>Click&Pledge Credit Card payment gateway is Disabled</b></p>',
			'#suffix' => '</div>',
		);
		$element['is_cc_enabled'] = [
			'#type' => 'hidden',
			'#value' => 'no',
		];
	}
    return $element;
	
  }


  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    //The JS library performs its own validation.
	$connection= \Drupal::database();
	$config = \Drupal::config('cnp.mainsettings');	
	//echo $config->get('cnp.cnp_accid');
	$values = $form_state->getValue($element['#parents']);
	//echo "<pre>";
	//print_r($values);
	//exit();
	$order_id = \Drupal::routeMatch()->getParameter('commerce_order')->id();
    $order = Order::load($order_id);
	
	if($values['is_cc_enabled']=="no")
	{
		$form_state->setError($element['is_cc_enabled'], t('Please enable Click&Pledge payment gateway to accept payments'));
		return false;
	}
	
	if($values['fef_payment_methods']=="Credit Card")
	{
		if($values['fef_payment_options']=="Recurring")
		{
			//print_r($values);
			
			if($values["fef_cnp_noof_payments"] != "Indefinite Recurring Only")
			{
				if($values['fef_cnp_noof_payments']<=0 && $values['fef_cnp_noof_payments']!="")
				{
					$form_state->setError($element['fef_cnp_noof_payments'], t('No.of payments should be more than 1'));
					return false;
				}
				else
				{
					$noofPays=$values['fef_cnp_noof_payments'];
					if($values['fef_recuring_type_option']== "Installment")
					{
						if($noofPays<=1)
						{
							$form_state->setError($element['fef_cnp_noof_payments'], t('Please enter Number of payments value between 2 to 998 for installment'));
							return false;
						}
						if($noofPays>=999)
						{
							$form_state->setError($element['fef_cnp_noof_payments'], t('Please enter Number of payments value between 2 to 998 for installment'));
							return false;
						}
					}
					else
					{
						if($noofPays<=1)
						{
							$form_state->setError($element['fef_cnp_noof_payments'], t('Please enter Number of payments value between 2 to 999 for Subscription'));
							return false;
						}
						if($noofPays>=1000)
						{
							$form_state->setError($element['fef_cnp_noof_payments'], t('Please enter Number of payments value between 2 to 999 for Subscription'));
							return false;
						}
					}
					//no.of payments values should not be more than max.no.of payments defiend in dashbaord
					if($config->get('cnp.cnp_recurring_no_of_payments_options')=="openfield")
					{
						$maxNo=$config->get('cnp.cnp_recurring_max_no_payment');
						//echo $maxNo;
						if($maxNo != "")
						{
							if($maxNo != 0)
							{
								if($maxNo<$noofPays)
								{
									$form_state->setError($element['fef_cnp_noof_payments'], t('Please enter Number of payments value between 2 to '.$maxNo.' for Subscription'));
									return false;
								}
							}
						}
					}
				}
			}
		}
		
		$accCards=explode("#",$config->get('cnp.cnp_payment_credit_card_options_hidden'),-1);
		$accCards[]="Maestro";
		$card_type = CreditCard::detectType($values['cardnumber']);
		//echo $card_type->getId();
		//echo $card_type->getLabel();
		//print_r($accCards);
		//exit($values['cardnumber']);
		if(!empty($card_type))
		{
			if(!in_array(ucfirst($card_type->getId()),$accCards))
			{
				$form_state->setError($element['cardnumber'], t($card_type->getLabel().' Credit Card is not supported'));
				return false;
			}
		}
		
		if (!$card_type) {
		  $form_state->setError($element['cardnumber'], t('You have entered a credit card number of an unsupported card type.'));
		  return;
		}
		if (!CreditCard::validateNumber($values['cardnumber'], $card_type)) {
		  $form_state->setError($element['cardnumber'], t('You have entered an invalid credit card number.'));
		}
		if (!CreditCard::validateExpirationDate($values['expiration']['month'], $values['expiration']['year'])) {
		  $form_state->setError($element['expiration'], t('You have entered an expired credit card.'));
		}
		if (!CreditCard::validateSecurityCode($values['security_code'], $card_type)) {
		  $form_state->setError($element['security_code'], t('You have entered an invalid CVV.'));
		}
		// Persist the detected card type.
		$form_state->setValueForElement($element['type'], $card_type->getId());
	}
	//zero payment validation - pre-authourization is checked
	//print_r($config->get('cnp.cnp_pre_auth'));
	if($config->get('cnp.cnp_pre_auth')==1 && $values['fef_payment_options']=="Recurring")
	{
		if(round($order->getTotalPrice()->getNumber())==0)
		{
			$form_state->setError($element['payment_errors'], t('Zero Payment not allowed with Recurring.Please select One Time Only'));
			return false;
		}
	}
	//pre-authourization is unchecked
	if($config->get('cnp.cnp_pre_auth')==0)
	{
		if(round($order->getTotalPrice()->getNumber())==0)
		{
			$form_state->setError($element['payment_errors'], t('Zero Payment not allowed.'));
			return false;
		}
	}
	
    //   // $security_key = $this->configuration['security_key'];
	//print_r($values);
	//exit();
	
  }


  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    
    // The payment gateway plugin will process the submitted payment details.
    $values = $form_state->getValue($element['#parents']);
    //   // $security_key = $this->configuration['security_key'];
	//print_r($values);
	//exit();
    $this->entity->card_type = $values['type'];
    $this->entity->card_number = substr($values['cardnumber'], -4);
    $this->entity->card_exp_month = $values['expiration']['month'];
    $this->entity->card_exp_year = $values['expiration']['year'];

  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    // Add the stripe attribute to the postal code field.
    $form['billing_information']['address']['widget'][0]['address_line1']['#attributes']['data-stripe'] = 'address_line1';
    $form['billing_information']['address']['widget'][0]['address_line2']['#attributes']['data-stripe'] = 'address_line2';
    $form['billing_information']['address']['widget'][0]['locality']['#attributes']['data-stripe'] = 'address_city';
    $form['billing_information']['address']['widget'][0]['postal_code']['#attributes']['data-stripe'] = 'address_zip';
    $form['billing_information']['address']['widget'][0]['country_code']['#attributes']['data-stripe'] = 'address_country';
    return $form;
  }
  /*public function displayPaymentForm(array $form, FormStateInterface $form_state)
  {
	 $ajax_response = new AjaxResponse();
	 $values = $form_state->getValue($form['#parents']);
	 $payMethod=$values['payment_information']['add_payment_method']['payment_details']['fef_payment_methods'];
	
	 if($payMethod=="Credit Card")
	 {
		$ajax_response->addCommand(new CssCommand('#cnp-credit-card-division', array('display' => 'block')));
		$ajax_response->addCommand(new CssCommand('#cnp-echeck-division', array('display' => 'none')));
		$ajax_response->addCommand(new CssCommand('#cnp-ref-division', array('display' => 'none')));
	 }
	 elseif($payMethod=="eCheck")
	 {
		$ajax_response->addCommand(new CssCommand('#cnp-credit-card-division', array('display' => 'none')));
		$ajax_response->addCommand(new CssCommand('#cnp-echeck-division', array('display' => 'block')));
		$ajax_response->addCommand(new CssCommand('#cnp-ref-division', array('display' => 'none')));
	 }
	 else
	 {
		$ajax_response->addCommand(new CssCommand('#cnp-credit-card-division', array('display' => 'none')));
		$ajax_response->addCommand(new CssCommand('#cnp-echeck-division', array('display' => 'none')));
		$ajax_response->addCommand(new CssCommand('#cnp-ref-division', array('display' => 'block')));
	 }
	 return $ajax_response;
	 
  }*/
	public function displayRecurringForm(array $form, FormStateInterface $form_state)
	{
		$ajax_response = new AjaxResponse();
		$values = $form_state->getValue($form['#parents']);
		$payOption=$values['payment_information']['add_payment_method']['payment_details']['fef_payment_options'];
		if($payOption=="Recurring")
		{
			$ajax_response->addCommand(new CssCommand('#fef_recurr_options_division', array('display' => 'block')));
		}
		else
		{
			$ajax_response->addCommand(new CssCommand('#fef_recurr_options_division', array('display' => 'none')));
		}
		return $ajax_response;
	}
	public function safeString( $str,  $length=1, $start=0 )
	{
		$str = preg_replace('/\x03/', '', $str); //Remove new line characters
		return substr( htmlspecialchars( ( $str ) ), $start, $length );
	}
}